import 'dart:io';

import 'package:firebase_admob/firebase_admob.dart';

 class AdMob {

  //static const String testDevice = 'f9259d49-dd69-4887-bf7d-5c08fa01073a';
  static const MobileAdTargetingInfo targetingInfo = MobileAdTargetingInfo(
    //testDevices: testDevice != null ? <String>[testDevice] : null,
    //keywords: <String>['foo', 'bar'],
    //contentUrl: 'http://foo.com/bar.html',
    childDirected: true,
    nonPersonalizedAds: true,
  );

  static String _bannerID = "ca-app-pub-9530069764203602/2943548299";
  static String _instenstialID = "ca-app-pub-9530069764203602/1821315536";

  static BannerAd _bannerAd;
  static NativeAd _nativeAd;
  static InterstitialAd _interstitialAd;

  static BannerAd _createBannerAd() {
    return BannerAd(
      // adUnitId: BannerAd.testAdUnitId,
      adUnitId: _bannerID,
      size: AdSize.banner,
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
        print("BannerAd event $event");
      },
    );
  }

  static InterstitialAd _createInterstitialAd() {
    return InterstitialAd(
      // adUnitId: InterstitialAd.testAdUnitId,
      adUnitId: _instenstialID,
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
        print("InterstitialAd event $event");
      },
    );
  }

  static NativeAd _createNativeAd() {
    return NativeAd(
      adUnitId: NativeAd.testAdUnitId,
      factoryId: 'adFactoryExample',
      targetingInfo: targetingInfo,
      listener: (MobileAdEvent event) {
        print("$NativeAd event $event");
      },
    );
  }

  static void inicializar() {
    //FirebaseAdMob.instance.initialize(appId: "ca-app-pub-9530069764203602~6882793302");
    FirebaseAdMob.instance.initialize(appId: FirebaseAdMob.testAppId);
  }

  static void dispose() {
    _bannerAd?.dispose();
    _nativeAd?.dispose();
    _interstitialAd?.dispose();
  }

  static loadBanner() {
    _bannerAd = _createBannerAd()..load();
  }

  static showBanner() {
    _bannerAd = _createBannerAd()..load();
    _bannerAd ??= _createBannerAd();
    _bannerAd
      ..load()
      ..show();
  }

  static showBannerWhithOfficet() {
    _bannerAd ??= _createBannerAd();
    _bannerAd
      ..load()
      ..show(horizontalCenterOffset: -50, anchorOffset: 100);
  }

  static removeBanner() {
    _bannerAd?.dispose();
    _bannerAd = null;
  }

  static loadInstenstial() {
    _interstitialAd?.dispose();
    _interstitialAd = _createInterstitialAd()..load();
  }

  static showInstenstial() {
    _interstitialAd?.show();
  }

  static showNative() {
    _nativeAd ??= _createNativeAd();
    _nativeAd
      ..load()
      ..show(
        anchorType: Platform.isAndroid
            ? AnchorType.bottom
            : AnchorType.top,
      );
  }

  static removeNative() {
    _nativeAd?.dispose();
    _nativeAd = null;
  }

  static leadRewardedVideo() {
    RewardedVideoAd.instance.load(
        adUnitId: RewardedVideoAd.testAdUnitId,
        targetingInfo: targetingInfo);
  }

  static showRewardedVideo() {
    RewardedVideoAd.instance.show();
  }
}