class MetaModel{

  MetaModel(this._valor, this._motivo, this._nomeTabela, this._isCompleted);

  //TODO: ACRESCENTAR O CONSTRUTOR E MEXER NO DB
  bool _isCompleted;

  bool get isCompleted => _isCompleted;

  set isCompleted(bool isCompleted) {
    _isCompleted = isCompleted;
  }

  String _valor;

  String get valor => _valor;

  set valor(String valor) {
    _valor = valor;
  }
  String _motivo;

  String get motivo => _motivo;

  set motivo(String motivo) {
    _motivo = motivo;
  }
  String _nomeTabela;

  String get nomeTabela => _nomeTabela;

  set nomeTabela(String nomeTabela) {
    _nomeTabela = nomeTabela;
  }
  String _id;

  String get id => _id;

  set id(String id) {
    _id = id;
  }

}