class DataModel{

  DataModel(this._valor, this._data, this._isSaldo, this._comentario);

  int _id;

  int get id => _id;

  set id(int id) {
    _id = id;
  }
  String _valor;

  String get valor => _valor;

  set valor(String valor) {
    _valor = valor;
  }
  String _data;

  String get data => _data;

  set data(String data) {
    _data = data;
  }
  String _comentario;

  String get comentario => _comentario;

  set comentario(String comentario) {
    _comentario = comentario;
  }
  bool _isSaldo;

  bool get isSaldo => _isSaldo;

  set isSaldo(bool isSaldo) {
    _isSaldo = isSaldo;
  }

}