import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:minha_meta_economica/screens/home_screen.dart';
import 'package:minha_meta_economica/localizations/intro_screen.i18n.dart';
import 'package:shared_preferences/shared_preferences.dart';

class IntroScreen extends StatefulWidget {
  @override
  _IntroScreenState createState() => _IntroScreenState();
}

class _IntroScreenState extends State<IntroScreen> {
  final introKey = GlobalKey<IntroductionScreenState>();
  SharedPreferences sharedPrefs;

  Future<void> _onIntroEnd(context) async {

    sharedPrefs = await SharedPreferences.getInstance();
    sharedPrefs.setBool('FirstOpen', false);
    
    Navigator.of(context).push(
      // MaterialPageRoute(builder: (_) => SignInPage()),
      MaterialPageRoute(builder: (_) => HomeScreen()),
    );
  }

  Widget _buildImage(String assetName) {
    return Align(
      child: Image.asset('assets/$assetName.png', width: 200.0),
      //child: Image.network("https://domaine.com/image.png", height: 350.0),
      alignment: Alignment.bottomCenter,

    );
  }

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 19.0);
    const pageDecoration = const PageDecoration(
      titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Colors.white,
      imagePadding: EdgeInsets.zero,
    );

    return IntroductionScreen(
      key: introKey,
      pages: [
        PageViewModel(
          title: "Economizar = Plantar".i18n,
          body:
              "Se você pensa no futuro, economize hoje, no presente.".i18n,
          image: _buildImage('img3'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Deixe de comprar para poder comprar".i18n,
          body:
              "Elimine os gastos desnecessarios para poder focar naquilo que sempre quis.".i18n,
          image: _buildImage('img2'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Descomplique".i18n,
          body:
              "Para economizar você precisa apenas de dedicação.".i18n,
          image: _buildImage('img4'),
          decoration: pageDecoration,
        ),
      ],
      onDone: () => _onIntroEnd(context),
      //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
      showSkipButton: true,
      skipFlex: 0,
      nextFlex: 0,
      skip:  Text('Pular'.i18n),
      next: const Icon(Icons.arrow_forward),
      done:  Text('Próximo'.i18n, style: TextStyle(fontWeight: FontWeight.w600)),
      dotsDecorator: const DotsDecorator(
        size: Size(10.0, 10.0),
        color: Color(0xFFBDBDBD),
        activeSize: Size(22.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
    );
  }
}