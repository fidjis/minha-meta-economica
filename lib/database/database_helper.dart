import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
//import 'package:path_provider/path_provider.dart';

class DatabaseHelper {  
  static final _databaseName = "DB14MetaEconomica.db";
  static final _databaseVersion = 1;
  static final tableMetas = 'tableMetas'; 
  static final columnId = '_id';
  static final columnValor = 'columnValor';
  static final columnIscompleted = 'columnIscompleted';
  static final columnMotivo = 'columnMotivo';
  static final columnNomeTabela = 'columnNomeTabela';
  static final columnData = 'columnData';
  static final columnIsSaldo = 'columnIsSaldo';
  static final columnComentario = 'columnComentario';
  // torna esta classe singleton
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();
  // tem somente uma referência ao banco de dados
  static Database _database;

  Future<Database> get database async {
    if (_database != null) return _database;
    // instancia o db na primeira vez que for acessado
    _database = await _initDatabase();
    return _database;
  }  
  // abre o banco de dados e o cria se ele não existir
  _initDatabase() async {
    //pathprovider cmetodo
    //Directory documentsDirectory = await getApplicationDocumentsDirectory();
    //String path = join(documentsDirectory.path, _databaseName);
    String path = join(await getDatabasesPath(), _databaseName);
    return await openDatabase(path,
        version: _databaseVersion,
        onCreate: _onCreate);
  }
  // Código SQL para criar o banco de dados e a tabela
  Future _onCreate(Database db, int version,) async {
    await db.execute('''
          CREATE TABLE $tableMetas (
            $columnId INTEGER PRIMARY KEY,
            $columnValor TEXT NOT NULL,
            $columnMotivo TEXT NOT NULL,
            $columnNomeTabela TEXT NOT NULL,
            $columnIscompleted BOOLEAN NOT NULL
          )
          ''');
  }  
  // métodos Helper
  //----------------------------------------------------
  Future createTable(String table) async {
    await _database.execute('''
          CREATE TABLE IF NOT EXISTS $table (
            $columnId INTEGER PRIMARY KEY,
            $columnValor TEXT NOT NULL,
            $columnData TEXT NOT NULL,
            $columnIsSaldo BOOLEAN NOT NULL,
            $columnComentario TEXT NOT NULL
          )
          ''');
  }  
  // Insere uma linha no banco de dados onde cada chave 
  // no Map é um nome de coluna e o valor é o valor da coluna. 
  // O valor de retorno é o id da linha inserida.
  Future<int> insert(Map<String, dynamic> row, String table) async {
    Database db = await instance.database;
    return await db.insert(table, row);
  }
  // Todas as linhas são retornadas como uma lista de mapas, onde cada mapa é
  // uma lista de valores-chave de colunas.
  Future<List<Map<String, dynamic>>> queryAllRows(String table) async {
    Database db = await instance.database;
    if(table != null)
      return await db.query(table);
  }
  // Todos os métodos : inserir, consultar, atualizar e excluir, 
  // também podem ser feitos usando  comandos SQL brutos. 
  // Esse método usa uma consulta bruta para fornecer a contagem de linhas.
  Future<int> queryRowCount(String table) async {
    Database db = await instance.database;
    return Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT(*) FROM $table'));
  }
  // Assumimos aqui que a coluna id no mapa está definida. Os outros
  // valores das colunas serão usados para atualizar a linha.
  Future<int> update(Map<String, dynamic> row, String table) async {
    Database db = await instance.database;
    //int id = row[columnId];
    String id = row[columnId];
    return await db.update(table, row, where: '$columnId = ?', whereArgs: [id]);
  }
  // Exclui a linha especificada pelo id. O número de linhas afetadas é
  // retornada. Isso deve ser igual a 1, contanto que a linha exista.
  Future<int> delete(int id, String table) async {
    Database db = await instance.database;
    return await db.delete(table, where: '$columnId = ?', whereArgs: [id]);
  }
}