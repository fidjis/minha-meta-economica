import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

    static var _t = Translations("pt_br") +
        {
          "pt_br": "Economizar = Plantar",
          "en_us": "Save = Plant",
        } +
        {
          "pt_br": "Se você pensa no futuro, economize hoje, no presente.",
          "en_us": "If you think about the future, save today, in the present.",
        } +
        {
          "pt_br": "Deixe de comprar para poder comprar",
          "en_us": "Stop shopping to be able to buy",
        } +
        {
          "pt_br": "Elimine os gastos desnecessarios para poder focar naquilo que sempre quis.",
          "en_us": "Eliminate unnecessary expenses so you can focus on what you’ve always wanted.",
        } +
        {
          "pt_br": "Descomplique",
          "en_us": "Uncomplicate",
        } +
        {
          "pt_br": "Para economizar você precisa apenas de dedicação.",
          "en_us": "To save you need only dedication.",
        } +
        {
          "pt_br": "Pular",
          "en_us": "Skip",
        } +
        {
          "pt_br": "Próximo",
          "en_us": "Done",
        };

  String get i18n => localize(this, _t);
}

