import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

    static var _t = Translations("pt_br") +
        {
          "pt_br": "Meta",
          "en_us": "Goal",
          //"es": "¿Hola! Cómo estás?",
          //"fr": "Salut, comment ca va?",
          //"de": "Hallo, wie geht es dir?",
        } +
        {
          "pt_br": "Valor atual:",
          "en_us": "Current value:",
        }  +
        {
          "pt_br": "Meu saldo",
          "en_us": "My balance",
        } +
        {
          "pt_br": "“Se você almeja ser rico, pense em poupar assim como você pensa em ganhar.” – Benjamin Franklin",
          "en_us": "“If you want to be rich, think about saving as you think about winning.” - Benjamin Franklin",
        } +
        {
          "pt_br": "Novo Registro",
          "en_us": "New Registration",
        } +
        {
          "pt_br": "Minhas Metas",
          "en_us": "My Goals",
        } +
        {
          "pt_br": "Configurações",
          "en_us": "Settings",
        } +
        {
          "pt_br": "Em Construção!",
          "en_us": "Under construction!",
        } +
        {
          "pt_br": "Aguarde as proximas atualizações.",
          "en_us": "Wait for the next updates.",
        } +
        {
          "pt_br": "Fechar",
          "en_us": "Close",
        } +
        {
          "pt_br": "Em Construção!",
          "en_us": "Under construction!",
        } +
        {
          "pt_br": "Politica de Privacidade",
          "en_us": "Privacy policy",
        } +
        {
          "pt_br": "Sobre",
          "en_us": "About",
        } +
        {
          "pt_br": "“Cuidado com as pequenas despesas”",
          "en_us": "“Watch out for small expenses”",
        } +
        {
          "pt_br": "Meta Econômica",
          "en_us": "Economic Goal",
        } +
        {
          "pt_br": "Ocorreu um erro ao carregar os dados",
          "en_us": "There was an error loading data",
        } +
        {
          "pt_br": "Adicione seus dados!",
          "en_us": "Add your details!",
        } +
        {
          "pt_br": "Atenção!",
          "en_us": "Attention!",
        } +
        {
          "pt_br": "Antes de registrar adicione uma meta!",
          "en_us": "Before registering add a goal!",
        } +
        {
          "pt_br": "Adicionar",
          "en_us": "Add",
        };

  String get i18n => localize(this, _t);
}

