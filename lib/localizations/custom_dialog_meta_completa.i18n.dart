import 'package:i18n_extension/i18n_extension.dart';

extension Localization on String {

    static var _t = Translations("pt_br") +
        {
          "pt_br": "Parabéns!",
          "en_us": "Congratulations!",
        } +
        {
          "pt_br": "Voce conseguiu completar o seu objetivo!",
          "en_us": "You managed to complete your goal!",
        };

  String get i18n => localize(this, _t);
}

